# Loading Packages ----

library(dplyr)
library(ggplot2)
library(nycflights13)

# Exploring the data ----

head(flights)
summary(flights)

# Pregunta N1 �Cu�l fue el peor d�a para viajar en 2013? ... Se asume como peor el d�a con mayor promedio  de aviones con retraso ----


flights %>%
  filter(arr_delay > 0) %>%
  group_by( year, month, day) %>%
  summarise(delay = mean(arr_delay, na.rm = TRUE)) %>%
  arrange(-delay)

"Respuesta: el 8 de marzo es el peor d�a para viajar al tener el mayor promedio de retrasos;
 Y si consideramos s�lo retrasos sin adelantos la respuesta es 10 de Julio con restrasos de cerca de 110min"

# Pregunta N2 �Cu�l es la peor aerolinea con respecto a retrasos? ----


flights %>%
    filter(arr_delay > 0) %>%
    group_by(carrier) %>%
    summarise(
      delay = mean(arr_delay, na.rm = TRUE),
    ) %>%
    arrange(-delay) %>%
    ggplot() + aes(x = carrier, y = delay) +
    geom_bar(stat = "identity")  

"Respuesta: La peor aerolinea quitando los adelantos de vuelo, es oo"


# Pregunta N3 �Cu�l es la mejor aerol�nea? Consideramos la mejor aerolinea como aquella que tiene un promedio de retrasos menor ----


flights %>%
  group_by(carrier) %>%
  summarise(
    delay = mean(arr_delay, na.rm = TRUE),
  ) %>%
  arrange(delay)

"Respuesta: La mejor aerolinea es AS"

# Pregunta N4 �Cu�l es el destino m�s frecuente? ----

flights %>%
  group_by(dest) %>%
  summarise( 
    N_Flights = n()
  ) %>%
  arrange (-N_Flights)

"El destino m�s frecuente es: Chicago (ORD)"

# Pregunta N5 �De d�nde ven�an la mayor cantidad de vuelos? ----

flights %>%
  group_by(origin) %>%
  summarise( 
    N_Flights_Origin = n()
  ) %>%
  arrange (-N_Flights_Origin)

"La mayor cantidad de vuelos vienen de Newark, (EWR)"

# Pregunta N6 �Cu�les son las 10 rutas m�s transitadas? ----

flights %>%
  mutate( routes = paste0(origin, "_", dest)) %>%
  group_by( routes)%>%
  summarise( 
    Flight_Route = n(),
    Avg_Dist = mean(distance)
    ) %>%
  arrange (-Flight_Route)
  
" La ruta m�s recorrida es JFK_LAX"

# Pregunta N7 �Qu� d�a de la semana es el peor para viajar? ----

flights %>%
  Dia_Sem <- weekdays(as.POSIXct(time_hour)) %>%
  group_by(Dia_Sem) %>%
  summarise(Delay = mean(arr_delay))
  
flights %>%
  mutate( Weekday = weekdays(as.Date(time_hour))) %>%
  group_by(Weekday) %>%
  summarise(Delay = mean(arr_delay, na.rm = TRUE)) %>%
  arrange(-Delay) %>%
  ggplot() + aes(x = Weekday, y = Delay) +
  geom_bar(stat = "identity") 

"El peor d�a para viajar es Jueves"























